<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY version SYSTEM "version.xml">
]>
<refentry id="milter-manager-Client-library">
<refmeta>
<refentrytitle role="top_of_page" id="milter-manager-Client-library.top_of_page">クライアントライブラリ</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>milter managerライブラリ</refmiscinfo>
</refmeta>
<refnamediv>
<refname>クライアントライブラリ</refname>
<refpurpose>milterを実装するためのクライアントライブラリ。</refpurpose>
</refnamediv>

<refsect1 id="milter-manager-Client-library.functions" role="functions_proto">
<title role="functions_proto.title">Functions</title>
<informaltable pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="functions_return" colwidth="150px"/>
<colspec colname="functions_name"/>
<tbody>
<row><entry role="function_type"><link linkend="void"><returnvalue>void</returnvalue></link>
</entry><entry role="function_name"><link linkend="milter-client-init">milter_client_init</link> <phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="void"><returnvalue>void</returnvalue></link>
</entry><entry role="function_name"><link linkend="milter-client-quit">milter_client_quit</link> <phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="GOptionGroup"><returnvalue>GOptionGroup</returnvalue></link> *
</entry><entry role="function_name"><link linkend="milter-client-get-option-group">milter_client_get_option_group</link> <phrase role="c_punctuation">()</phrase></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>


<refsect1 id="milter-manager-Client-library.description" role="desc">
<title role="desc.title">説明</title>
<para>milter-clientライブラリはクライアント側のすべてのmilterプロトコルを処理する機能を提供します。「クライアント側」というのは「milter側」ということで、「MTA側」ではありません。</para>
<para>milter-clientライブラリでの主要なクラスは<link linkend="MilterClient"><literal>MilterClient</literal></link>と<link linkend="MilterClientContext"><literal>MilterClientContext</literal></link>です。<link linkend="MilterClient"><literal>MilterClient</literal></link>はMTAからの複数の接続を受け付け、各接続は<link linkend="MilterClientContext"><literal>MilterClientContext</literal></link>が処理します。</para>
<refsect3><title>使用法概観</title><para>You need to set connection spec and connect
<link linkend="MilterClient-connection-established"><type>“connection-established”</type></link> signal before
entering mail loop.</para>
<para>接続指定はMTAが<link linkend="MilterClient"><literal>MilterClient</literal></link>に接続するためのエントリポイントです。接続指定は「プロトコル:情報」という形式になっています。IPv4ソケットでは「inet:ポート番号」、「inet:ポート番号&commat;ホスト名」、「inet:ポート番号&commat;[IPv4アドレス]」が正しい形式です。IPv6ソケットでは、「inet6:ポート番号」、「inet6:ポート番号&commat;ホスト名」、「inet6:ポート番号&commat;[IPv6アドレス]」が正しい形式です。UNIXドメインソケットでは「unix:パス」が正しい形式です。</para>
<para><link linkend="MilterClient"><literal>MilterClient</literal></link> emits <link linkend="MilterClient-connection-established"><type>“connection-established”</type></link>
signal. <link linkend="MilterClientContext"><literal>MilterClientContext</literal></link> setup should be done in the
signal. In many cases, you just connect callbacks to
the passed <link linkend="MilterClientContext"><literal>MilterClientContext</literal></link>. See <link linkend="MilterClientContext"><literal>MilterClientContext</literal></link>
for available signals.</para>
<para>milter_client_run() is running main loop function. You
can enter into main loop after you finish to prepare your
<link linkend="MilterClient"><literal>MilterClient</literal></link>.</para>
<para>milter-clientライブラリを使った例です。tool/milter-test-client.cも見てください。milter-test-client.cはmilter-clientライブラリを使ったmilterの実装例です。</para>
<informalexample><programlisting role="example"><![CDATA[
#include <stdlib.h>
#include <milter/client.h>

static void
cb_connection_established (MilterClient *client,
                           MilterClientContext *context,
                           gpointer user_data)
{
   connect_to_your_interesting_signals(client, context, user_data);
}

int
main (int argc, char **argv)
{
    gboolean success;
    const gchar spec[] = "inet:10025@localhost";
    MilterClient *client;
    GError *error = NULL;

    milter_init();
    milter_client_init();

    client = milter_client_new();
    if (!milter_client_set_connection_spec(client, spec, &error)) {
        g_print("%s\n", error->message);
        g_error_free(error);
        return EXIT_FAILURE;
    }
    g_signal_connect(client, "connection-established",
                     G_CALLBACK(cb_connection_established), NULL);
    milter_client_run(client);

    milter_client_quit();
    milter_quit();

    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}
]]></programlisting></informalexample>
<para/>
</refsect3>
<refsect3><title>処理モデル</title><para>Sendmailが提供するlibmilterは各接続毎にスレッドを生成するモデルです。しかし、milter-clientライブラリはこのモデルを使いません。milter-clientライブラリは2つのスレッドを使います。1つのスレッドは<link linkend="MilterClient"><literal>MilterClient</literal></link>用のスレッドで、もう1つのスレッドは<link linkend="MilterClientContext"><literal>MilterClientContext</literal></link>用のスレッドです。1つ目のスレッドでは<link linkend="MilterClient"><literal>MilterClient</literal></link>がMTAからの接続を受け付けて、それを割り振るだけです。<link linkend="MilterClientContext"><literal>MilterClientContext</literal>GMainLoop</link></para>
<para>libmilterのモデルは接続を受け付けるためにmilter-clientライブラリのモデルよりも多くコストがかかります。これは、libmilterのモデルがスレッドを生成するのに対して、milter-clientライブラリは単に<link linkend="MilterClientContext"><literal>MilterClientContext</literal></link>オブジェクトを生成するだけだからです。ただし、多くの場合、この違いはボトルネックとはなりません。:-|</para>
</refsect3>

</refsect1>
<refsect1 id="milter-manager-Client-library.functions_details" role="details">
<title role="details.title">Functions</title>
<refsect2 id="milter-client-init" role="function">
<title>milter_client_init ()</title>
<indexterm zone="milter-client-init"><primary>milter_client_init</primary></indexterm>
<programlisting language="C"><link linkend="void"><returnvalue>void</returnvalue></link>
milter_client_init (<parameter><type>void</type></parameter>);</programlisting>
<para>Call this function before using any other the milter-client
library functions.</para>
</refsect2>
<refsect2 id="milter-client-quit" role="function">
<title>milter_client_quit ()</title>
<indexterm zone="milter-client-quit"><primary>milter_client_quit</primary></indexterm>
<programlisting language="C"><link linkend="void"><returnvalue>void</returnvalue></link>
milter_client_quit (<parameter><type>void</type></parameter>);</programlisting>
<para>Call this function after the milter-client library use.</para>
</refsect2>
<refsect2 id="milter-client-get-option-group" role="function">
<title>milter_client_get_option_group ()</title>
<indexterm zone="milter-client-get-option-group"><primary>milter_client_get_option_group</primary></indexterm>
<programlisting language="C"><link linkend="GOptionGroup"><returnvalue>GOptionGroup</returnvalue></link> *
milter_client_get_option_group (<parameter><link linkend="MilterClient"><type>MilterClient</type></link> *client</parameter>);</programlisting>
<para>Gets a <link linkend="GOptionGroup"><literal>GOptionGroup</literal></link> for the <parameter>client</parameter>
. The option group
has common milter client options.</para>
<refsect3 id="milter-client-get-option-group.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>client</para></entry>
<entry role="parameter_description"><para><link linkend="MilterClient"><literal>MilterClient</literal></link>。</para></entry>
<entry role="parameter_annotations"/></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="milter-client-get-option-group.returns" role="returns">
<title>Returns</title>
<para> a new <link linkend="GOptionGroup"><literal>GOptionGroup</literal></link> for <parameter>client</parameter>
.</para>
</refsect3></refsect2>

</refsect1>
<refsect1 id="milter-manager-Client-library.other_details" role="details">
<title role="details.title">Types and Values</title>

</refsect1>

</refentry>
